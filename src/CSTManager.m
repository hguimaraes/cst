%% CSTManager Function
%
%  This function is the main kernel of the process. It commands all the flow
% of information and request for function, writing process and everything
% else.
%
function [] = CSTManager(filepath)
    
    % Read the frames from the file
    disp('PHASE 1 - Reading Image Frames!')
    imgFrames = frames2Stack(filepath);
    
    % Use the Optical Flow algorithm in the Images
    disp('PHASE 2 - Using Background Subtraction')
    alphavalue = 0.5;
    Threshold = 15;
    %backgroundSubtrack = AdaptativeBackgroundSubtrack('../assets',imgFrames,alphavalue,Threshold);
    backgroundSubtrack = AdaptativeBackgroundSubtrack2(filepath,imgFrames,alphavalue,Threshold);
    
    % Use the Kmeans Algorithm in the previous result 
    disp('PHASE 3 - K-means Segmentation')
    numOfCluster = 4;
    seeds = [255 255 255; 30 50 255; 130 150 50; 0 0 0];
    ClusterA = cell(1,length(backgroundSubtrack));
    ClusterB = cell(1,length(backgroundSubtrack));
    
    for i=1:numOfCluster
        clusterName = sprintf('Cluster-%d/',i);
        newPath = strcat(filepath,'/KMeans/',clusterName);
        if ~exist(newPath,'dir')
            mkdir(newPath);
        end
    end
    
    for i=1:length(backgroundSubtrack)
    
        [kmeansClusters, newSeed] = RGBKmeans(backgroundSubtrack{i}, numOfCluster, seeds);

        imgOut = repmat(kmeansClusters,[1 1 1]);
        img = rgb2gray(backgroundSubtrack{i});
        
        for j=1:numOfCluster
            clusterName = sprintf('Cluster-%d/',j);
            newPath = strcat(filepath,'/KMeans/',clusterName);
            imgName = sprintf('Image-%5.5d.tif',i);
            disp(strcat(clusterName,'-',imgName))
                                   
            temp = imgOut; 
            temp(temp ~= j) = 0; 
            temp(temp == j) = 1;
            imageKmeans = img.*uint8(temp);
            imwrite(imageKmeans,strcat(newPath,imgName));
            
            temp = logical(temp);
            if (j == 1)
                ClusterA{i} = temp;
            end
            if(j == 2)
                ClusterB{i} = temp;
            end
            
        end
    end
  
    % Mask and Clustering
    disp('PHASE 4 - Clustering objects and Morphology')
           
    maskA = cell(1,length(backgroundSubtrack));
    maskB = cell(1,length(backgroundSubtrack));
    clusteringA = cell(1,length(backgroundSubtrack));
    clusteringB = cell(1,length(backgroundSubtrack));
    
    for i=1:length(backgroundSubtrack)
        maskA{i} = imfill(imclose(imopen(ClusterA{i}, strel('rectangle', [3,3])),...
                                    strel('rectangle', [15, 15])),'holes');
        maskB{i} = imfill(imclose(imopen(ClusterB{i}, strel('rectangle', [3,3])),...
                                    strel('rectangle', [15, 15])),'holes');
                                
                                
                                
        maskAname = sprintf('/MASK/ClusterA/ClusterA-%5.5d.tif',i);
        maskBname = sprintf('/MASK/ClusterB/ClusterB-%5.5d.tif',i);
        imwrite(maskA{i},strcat(filepath,maskAname));
        imwrite(maskB{i},strcat(filepath,maskBname));
        
        
        clusteringA{i} = bwlabel(maskA{i},8);
        clusteringB{i} = bwlabel(maskB{i},8);
    end
   
    % Draw the Results
%     for i=1:length(clusteringA)
%         
%         BoudingBoxClusterA{i}
%         BoudingBoxClusterB{i}
%         
%     end
    
end