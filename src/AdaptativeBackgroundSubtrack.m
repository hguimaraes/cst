%% AdaptativeBackgroundSubtrack Function
%
%  This function implements the Background Subtracking by using a difference
% of Background and the current image frame. The background is recalculated
% for each new frame. In this function the value of the Threshold is 
% calculated for each new frame.
%
function subtractM = AdaptativeBackgroundSubtrack(filepath,imgIN, alphaValue,ThresholdValue)
    
    % Useful variables
    imgQT           = length(imgIN);
    subtractM       = cell(1,imgQT);
    background      = cell(1,imgQT);
    Threshold       = cell(1,imgQT);
    t               = 1;
    
    %
    background{1} = rgb2gray(imgIN{1});
    Threshold{1}  = ones(size(rgb2gray(imgIN{1})))*ThresholdValue;
    % Masked subtraction
    while(t ~= (imgQT+1))
        subtractM{t} = imsubtract(rgb2gray(imgIN{t}),background{t});
        subtractM{t}(subtractM{t} <= Threshold{t}) = 0;
        subtractM{t}(subtractM{t} > Threshold{t}) = 1;
        ImageName = sprintf('Image-%5.5d.tif', t);
        imwrite(logical(subtractM{t}),strcat(filepath,'/OpticalFlow/Mask/',ImageName),'tiff');
        
        
        subtractM{t} = repmat(subtractM{t},[1 1 3]).*imgIN{t};
        imwrite(subtractM{t},strcat(filepath,'/OpticalFlow/RESULT/',ImageName),'tiff');
        
        t = t+1;

        background{t} = alphaValue*background{t-1} + (1-alphaValue)*rgb2gray(imgIN{t-1});
        Threshold{t} = double(alphaValue*Threshold{t-1}) + double((1-alphaValue)*5*rgb2gray(subtractM{t-1}));
        disp(ImageName);
    end
end