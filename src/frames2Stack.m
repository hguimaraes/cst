%% frames2Stack function
%
%  This function receive as argument a directory path to a folder of frames
% and return a Cell Of a RGB Stack of Images to begin the processing.
%

function Stack = frames2Stack(filepath)
    
    % Get a cell list with name of the images
    x = dir(strcat(filepath,'/OriginalFrames/*.tif'));
    imgList = cell(1,length(x));
    for i=1:length(x)
        imgList{i} = x(i).name; 
    end
    
    % Useful variables and extracting the images size
    Stack = cell(1,length(imgList));
    
    % Filling the Stack
    for k=1:length(imgList)
        Stack{k} = imread(strcat(filepath,'/OriginalFrames/',imgList{k}));
    end
end