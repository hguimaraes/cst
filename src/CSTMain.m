% -------------------------------------------------------------------------
% Program       : Clustering Soccer Teams (CSTMain)
% Author        : Heitor R. Guimaraes
% E-mail        : hguimaraes@{cbpf.br or poli.ufrj.br}
% Copyright(c)  : Universidade Federal do Rio de Janeiro (UFRJ)
% Version       : v0.9
% Objectives    :   This softwarte has the objective of clusterize soccer
%                 players in a soccer video. The result will be a scene of
%                 the game in wich the algorithm will indicate the players
%                 of different teams.
%
% How to use    :   Call this function using the parameters file name
%                (string), file path, time of the video to start analyse 
%                and time to stop.
% Ex.           : CST(../assets,"Bardcelona-cutted",0,15)  
%
% License       :
%
% The MIT License (MIT)
% 
% Copyright (c) 2015 Heitor Guimaraes
% 
%  Permission is hereby granted, free of charge, to any person obtaining a 
% copy of this software and associated documentation files(the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the 
% Software is furnished to do so, subject to the following conditions:
% 
%  The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% 
%  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
% IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY 
% CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
% TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
% SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
%--------------------------------------------------------------------------
 
function [] = CSTMain(varargin)
    
    parameters = varargin;
    % function to coordinate the program flow
    try 
        %  Preparing the environment, creating folders and check errors
        % in the arguments.
        filePath = IOManager(parameters);
        %  Controling the program flow.
        CSTManager(filePath);
    catch ME
        disp(['Error ID: ' ME.identifier])
        rethrow(ME)
    end
end