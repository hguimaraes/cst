%% IOManager Function
%
% Function to handle the command line arguments and create or destroy
% folders for the process.
%
function filepath = IOManager(parameters)
    if length(parameters) ~= 1
        ME = MException('MyComponent:noSuchVariable', ...
        'Invalid number of arguments. Please Check out!');
        throw(ME)
    end
    % Checking if the arguments are valids
    filepath  = parameters{1}; 
    
    if ~exist(filepath,'dir')
        ME = MException('MyComponent:noSuchVariable', ...
        'Undefinied Input parameters. Please Check out!');
        throw(ME);
    end
    
    %  Creating the folders to Receive the frames form each part of the
    % processing algorithm.
    folders = {'/OriginalFrames','/OpticalFlow','/KMeans','/Result','/MASK',...
        '/OpticalFlow/Mask','/OpticalFlow/RESULT','/MASK/ClusterA','/MASK/ClusterB'};
    
    for i=1:length(folders)
        newPath = strcat(filepath,folders{i});
        if ~exist(newPath,'dir')
            mkdir(newPath);
        end
    end
end