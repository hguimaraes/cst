%% medianFilter function
%
%  This function receive as argument a RGB Image and returns
% other RGB Image processed by a median filter. 
%
% More details about the filter can be found at :
%   http://en.wikipedia.org/wiki/Median_filter

function ImagesOut = medianFilter(ImagesIn, kernelSize)

    % Pre-allocating memory for the image Output
    ImagesOut = ImagesIn;
    
    % Check kernelSize
    if gcd(size(ImagesIn),size(ImagesIn)) < kernelSize
        disp('The kernel is too big for this image!');
        return
    end
    
    % Median filter for a determinate kernelSize
    for imgCount = 1: size(ImagesIn,3)
        for x = 1 : size(ImagesIn(:,:,imgCount), 2) - (kernelSize-1)
            for y = 1 : size(ImagesIn(:,:,imgCount), 1) - (kernelSize-1)
                kernel = ImagesIn(y : y + (kernelSize-1),...
                                  x : x + (kernelSize-1),...
                                  imgCount);
                ImagesOut(y,x,imgCount) = median(kernel(:));
            end
        end
    end
    
    % Deallocating memory
    clear imgCount;
    clear ImagesIn;
    clear kernel;
end