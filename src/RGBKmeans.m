%% RGBKmeans Functions
%
%  This function is a implementation of the K-means algorithm for a RGB
%  Space. Receive as arguments the Image, the number of Clusters to segment
%  and the seeds for  theses clusters. Return as output the new seeds after
%  the recalculation of the centroids multiple times and the image
%  segmented.
%
function [KmeansCluster, NewSeeds] = RGBKmeans(imageIn, numOfCluster, seed)
    [m,n,p] = size(imageIn);
    imageIn = double(imageIn);
    
    r = imageIn(:,:,1);
    g = imageIn(:,:,2);
    b = imageIn(:,:,3);
    
    vecR = repmat(r(:)-min(r(:))+1,[1,numOfCluster]);
    vecG = repmat(g(:)-min(g(:))+1,[1,numOfCluster]);
    vecB = repmat(b(:)-min(b(:))+1,[1,numOfCluster]);
        
    seedR = seed(:,1)';
    seedG = seed(:,1)';
    seedB = seed(:,1)';
    
    iterator = 0;
    num = length(vecR);
    
    while(true)
            
        iterator = iterator+1;
        oldR   = seedR;
        oldG   = seedG;
        oldB   = seedB;
            
        meanR = repmat(seedR,[num,1]);
        meanG = repmat(seedG,[num,1]);
        meanB = repmat(seedB,[num,1]);
            
        clear meanred, clear meangreen, clear meanblue;
            
        distance = sqrt((vecR-meanR).^2 + ...
                        (vecG-meanG).^2 + ...
                        (vecB-meanB).^2);
            
        [~,label_vector] = min(distance,[],2);
            
        for i=1:numOfCluster
            index=(label_vector==i);
            seedR(:,i)=ceil(mean(vecR(index)));
            seedG(:,i)=ceil(mean(vecG(index)));
            seedB(:,i)=ceil(mean(vecB(index)));
        end
            
        if (seedR == oldR & seedG == oldG & seedB == oldB | iterator >50) %#ok<OR2,AND2>
            break;
        end
    end
        
    KmeansCluster = reshape(label_vector,[m,n]);
    NewSeeds = [seedR;seedG;seedB];
end
