# Clustering Soccer Teams (CST)

It is my final work of the course "Image Processing" (COS756) at UFRJ.
More details about the course can be found at (in Portuguese):
http://www.lcg.ufrj.br/Cursos/cos756/introducao-ao-processamento-de-imagens

### Objectives

Given a video of a soccer match, the algorithm should cluster and separate the teams. It uses the K-means algorithm in the RGB space to separate the teams and an algorithm to check what objects are moving in a scene (Background Subtraction).

## Results
Original Image
<center>![Original model](https://github.com/Hguimaraes/CST/blob/master/screenshots/Image-00005.png)</center>

Image after K-means (Result from one of the Clusters - Real Madrid Cluster)
<center>![K-means Result](https://github.com/Hguimaraes/CST/blob/master/screenshots/Cluster1.png)</center>

### How to use
* ./CST filename.avi -> To extract the Image frames.
* CSTMain ../assets  -> To run the main program in matlab

### Dependencies
* MATLAB R2014a
* OpenCV 2.4.11
* FFMPEG

### Author
* Heitor R. Guimarães
