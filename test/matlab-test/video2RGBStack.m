%% video2RGBStack Function
%
%  This function receive as argument a directory path to an video, 
% the filename the time to start the frames capture and time to end the
% capture in seconds. The function will return a 4D stack of RGB images
% to the developer.

function StackedImages = video2RGBStack(PathToVideo,filename,timeStart,timeEnd)
    
    % Read the video and get information about the file
    videoObj = VideoReader(strcat(PathToVideo,'/',filename));
    vidWidth = videoObj.Width;
    vidHeight = videoObj.Height;
    
    % Create a movie structure
    mov = struct('cdata',zeros(vidHeight,vidWidth,3,'uint8'),...
    'colormap',[]);

    % Create a folder to save the frames
    newPath = './OriginalFrames';
    if ~exist(newPath,'dir')
        mkdir(newPath);
    else
        rmdir(newPath,'s');
        mkdir(newPath);
    end
    
    % Saving Images and creating the 4D stack of RGB images
      % timeStart of the video
    videoObj.CurrentTime = timeStart; 
      % Ellapsed time choosed by the programmer
    sizeList = videoObj.CurrentTime - timeEnd;
      % Preallocating memory for the RGB stack
    StackedImages = uint8(zeros(vidHeight,vidWidth,3,sizeList));
    
    k = 1;
    while videoObj.CurrentTime <= timeEnd
        % Reading frame
        mov(k).cdata = readFrame(videoObj);
        
        % Writing the images and allocating the images into the array
        ImageName = sprintf('Image-%4.4d.tif', k);
        imwrite(mov(k).cdata,strcat(newPath,'/',ImageName),'tiff');
        StackedImages(:,:,:,k) = mov(k).cdata;
        
        % Deallocating memory to not get out of memory
        clear mov(k).cdata;
        
        % Next frame
        k = k+1;
    end
    
  % Deallocating memory
    clear mov;
    clear videoObj;
end