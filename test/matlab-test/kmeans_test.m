clear all, close all, clc

img = imread('../assets/OriginalFrames/Image-0062.tif');

%seeds = [255 255 255; 131 29 28; 95 120 65; 75 105 40];
seeds = [35 40 50; 255 255 255; 95 120 65];
numOfCluster = 3;

[clusters,seed] = RGBKmeans(img,numOfCluster,seeds); 
images = cell(1,numOfCluster);

imgOut = repmat(clusters,[1 1 3]);

for i=1:numOfCluster 
    temp = imgOut; 
    temp(temp~=i) = 0; 
    images{i} = img.*uint8(temp);  
end

imwrite(images{1},'Cluster1.tif','tiff');
imwrite(images{2},'Cluster2.tif','tiff');
imwrite(images{3},'Cluster3.tif','tiff');
%imwrite(images{4},'Cluster4.tif','tiff');

imshow(images{1});figure;
imshow(images{2});figure;
imshow(images{3});figure;
%imshow(images{4});