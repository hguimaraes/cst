#ifndef _CSTMANAGER_HPP_
#define _CSTMANAGER_HPP_

// C++ Headers
#include <exception>
#include <iostream>
#include <cstring>
#include <vector>

// OpenCV Headers
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

// Personal Headers
#include "VideoFrames.hpp"

namespace CST {
class CSTManager {
	std::string filename;
	unsigned tStart, tEnd;
	std::vector<cv::Mat> originalImgs;
	std::vector<cv::Mat> opticalFlowResult;
	std::vector<cv::Mat> kmeansResult;

	VideoFrames *imgStack = NULL;

public:
	CSTManager(int argc,char **argv);
	virtual ~CSTManager();
	void manage();

private:
	void allocatePtr();
};
}

#endif