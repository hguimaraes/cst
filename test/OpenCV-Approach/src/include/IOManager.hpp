#ifndef _IOMANAGER_HPP_
#define _IOMANAGER_HPP_

#include <exception>

namespace CST{
class IOManager {
public:
	IOManager(int argc, char **argv);
	virtual ~IOManager();
};
}

#endif