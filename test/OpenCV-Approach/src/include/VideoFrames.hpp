#ifndef _VIDEOFRAMES_HPP_
#define _VIDEOFRAMES_HPP_

// OpenCV Headers
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"

// C++ Headers
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstring>
#include <vector>

using namespace cv;

namespace CST{
class VideoFrames {
	VideoCapture *video;
	double videoFPS;

public:
	VideoFrames(std::string filename);
	virtual ~VideoFrames();
	std::vector<Mat> getFrames();
	double getFPS();
};
}

#endif