#include "VideoFrames.hpp"

namespace CST{
VideoFrames::VideoFrames(std::string filename){
	
	video = new VideoCapture(filename);
	
	if (!video->isOpened())
    	throw std::invalid_argument(" Error Opening the file. Please review the filename or check if ffmpeg is enable.");
    	
}

VideoFrames::~VideoFrames(){
	delete video;
}

std::vector<Mat> VideoFrames::getFrames(){
	std::vector<Mat> imgArray;

	std::stringstream newfileName;

	while(true) {
		Mat frame;
		
		if (!video->read(frame))
			break;
        
		imgArray.push_back(frame);
	}

	for (int i = 0; i < imgArray.size(); i++){
		newfileName << "OriginalFrames/OriginalFrame-" << std::setfill('0') << std::setw(5) << i << ".tif";
		std::cout<< newfileName.str() << std::endl;
		imwrite(newfileName.str(),imgArray[i]);
		newfileName.str("");
	}

	return imgArray;
}

double VideoFrames::getFPS(){
	return video->get(CV_CAP_PROP_FPS);
}

}