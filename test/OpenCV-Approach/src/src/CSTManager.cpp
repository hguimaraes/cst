#include "CSTManager.hpp"
namespace CST{

CSTManager::CSTManager(int argc, char **argv)
{
	try {
		filename 	= argv[1];
	} catch(std::exception& e) {
		std::cout << e.what() << std::endl;
	}
}

CSTManager::~CSTManager(){
	delete imgStack;
}

void CSTManager::allocatePtr(){
	imgStack = new VideoFrames(filename);
}

void CSTManager::manage(){
	allocatePtr();

	//  Read the Video and extract the slices.
	// Next Write the images and return an array of RGB images
	//
	originalImgs = imgStack->getFrames();
	 	
}

}