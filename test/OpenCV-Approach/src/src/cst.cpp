// C++ Headers
#include <iostream>
#include <cstring>

// Personal Headers
#include "IOManager.hpp"
#include "CSTManager.hpp"

int main(int argc, char **argv){
	try {
		CST::IOManager(argc,argv);
		
		CST::CSTManager softwareManager(argc,argv);
		softwareManager.manage();
	
	} catch(std::exception& e) {
		std::cout << e.what() << std::endl;
	}

	return 0;
}